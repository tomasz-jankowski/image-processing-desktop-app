<div align="center">

  <h1>Image processing desktop app</h1>

  <a href="https://nodejs.org/"><img alt="Node.js" src="https://img.shields.io/badge/node.js-12.18.0-%23404d59.svg?logo=node.js&logoColor=white" height="20"/></a>
    <a href="https://www.python.org/"><img alt="Python" src="https://img.shields.io/badge/python-3.7-%23404d59.svg?logo=python&logoColor=white" height="20"/></a>
  <a href="https://www.electronjs.org/"><img alt="Electron" src="https://img.shields.io/badge/Electron-%23404d59.svg?logo=Electron&logoColor=%2361DAFB" height="20"/></a>
    <a href="https://numpy.org/"><img alt="NumPy" src="https://img.shields.io/badge/numpy-%23404d59.svg?logo=numpy&logoColor=white" height="20"/></a>
    <a href="https://pillow.readthedocs.io/en/stable/"><img alt="Matplotlib" src="https://img.shields.io/badge/PIL-%23404d59.svg?logo=PIL&logoColor=white" height="20"/></a>
    <a href="https://matplotlib.org/"><img alt="Matplotlib" src="https://img.shields.io/badge/Matplotlib-%23404d59.svg?logo=Matplotlib&logoColor=white" height="20"/></a>

  <p><b>Desktop application to perform low-level image operations.</b></p>

  </br>

  <sub>University project. The application allows you to choose any JPEG or PNG image file and perform one of 13 different low-level operations available. After the operation, several images are presented, along with image parameters and histograms of the inital and final images. Application uses Python as a subprocess.<sub>

</div>


![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Installation

### Python installation

#### Automated

Method works with Windows only.

Run below scripts:
* `setup/setup-python.cmd` - Python installation
* `setup/setup-libraries.cmd` - libraries installation

#### Manual

* Install Python 3.7 and add it to PATH.
* Install below libraries:
    * cycler==0.10.0
    * decorator==4.4.2
    * imageio==2.8.0
    * kiwisolver==1.2.0
    * matplotlib==3.2.1
    * networkx==2.4
    * numexpr==2.7.1
    * numpy==1.18.2
    * Pillow==7.1.1
    * pyparsing==2.4.7
    * python-dateutil==2.8.1
    * PyWavelets==1.1.1
    * scikit-image==0.16.2
    * scipy==1.4.1
    * six==1.14.0
    * tables==3.6.1

### Dekstop app installation

```bash
$ cd src
$ npm install
$ cd express-app
$ npm install
```

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Usage

### Window application

```bash
$ cd src
$ npm start
```

### Browser

```bash
$ cd src/express-app/bin
$ node www
```

<b>WARNING:</b> To smoothly change between the Electron window and browser startup, you need to refactor paths in Python scripts (api.py, basic_operations.py).

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Python API

For more information, please refer to [Marcin Nawrocki's repository](https://github.com/MarcinNawrocki/NumPy-Image-App)

Python scripts are in src/express-app/public/python directory.
* `api.py` - script that allows efficient communication between Node and Python.
* `basic_operations.py` - script to handle basic operations, such as image loading/saving.
* `binary_operations.py` - functions that allow operating on binary images and transforming to binary images.
* `filtering.py` - filtration operations (matrix, median, gamma correction).
* `noising.py` - noises operations (salt-pepper, Gauss).

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Application walkthrough and screenshots

Show basic information about given image and choose operation (sample images are places inside *sample images* directory):
![scr1](https://user-images.githubusercontent.com/48838669/85070869-dbeb0c00-b1b6-11ea-8d76-55e35c80009f.PNG)

Operation being handled:
![scr2](https://user-images.githubusercontent.com/48838669/85070868-db527580-b1b6-11ea-99b3-cedf63ddd641.PNG)

Show original and final images parameters:
![scr3](https://user-images.githubusercontent.com/48838669/85070867-dab9df00-b1b6-11ea-9998-ce492fea481e.PNG)

Show histograms for original and final images:
![scr4](https://user-images.githubusercontent.com/48838669/85070864-da214880-b1b6-11ea-91a2-94535c92ac0d.PNG)

After returning to the home screen, final image is saved inside *src/express-app/public/python/output* directory.

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Credit

* [Marcin Nawrocki](https://github.com/MarcinNawrocki) - Python scripts
* [frankhale](https://github.com/frankhale) - [integrating Electron with Express](https://github.com/frankhale/electron-with-express)

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## License

Licensed under [MIT](https://opensource.org/license/mit/).
